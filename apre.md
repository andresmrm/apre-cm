---
title: Cuidando e Monitorando
subtitle: ferramentas livres para engajamento cidadão
date: 30/07/2018
lang: pt-BR
author: Andrés M. R. Martano
theme: solarized
---

# Colab - USP

[Colaboratório de Desenvolvimento e Participação](https://colab.each.usp.br)

Localizado na Escola de Artes Ciências e Humanidades (EACH-USP), tem entre seus objetivos:

- Promover o maior acesso à informação pública
- Promover transparência, accountability e participação social

# Cuidando do Meu Bairro

Plataforma, desenvolvida desde 2011 pelo [Colab-USP](https://colab.each.usp.br), que mapeia o orçamento da cidade de São Paulo-SP, facilitando inclusive o envio de pedidos de acesso à informação.

[cuidando.vc](https://cuidando.vc)

---

- Usa dados de execução orçamentária disponibilizados pela prefeitura
- Parceria com o [Observatório Social de São Paulo](http://saopaulo.osbrasil.org.br/)
- Dezenas de oficinas (Escola de Cidadania, Escola do Parlamento, Agente de Governo Aberto, Observatório Social)
- Prêmio de Educação Fiscal 2016 (FEBRAFITE)

---

![](imgs/cuidando.png){height=500px}

---

![](imgs/cuidando2.png){height=500px}

---

![](imgs/escola_cid.jpg){height=500px}

---

![](imgs/escola_parl.jpg){height=500px}

---

![](imgs/obsp.jpg){height=500px}

---

![](imgs/cuidando_arq.svg){height=600px}

# Monitorando a Cidade

Plataforma e metodologia para criação de [campanhas cívicas](http://promisetracker.org) de coleta de dados, desenvolvido pelo [Center for Civic Media (MIT)](https://civic.mit.edu).

[monitorandoacidade.org](http://monitorandoacidade.org)

---

- Surge da vontade de monitorar o Programa de Metas municipal: [De Olho nas Metas](https://deolhonasmetas.org.br)
- Percebem a importância de deixar as pessoas escolherem o que monitorar

---

- Site para criação de campanhas
- Coleta de dados via celular
- Relatório automático
- Metodologia participativa
- Parceria com Colab-USP

---

- 17 oficinas ao redor do Brasil
- Campanhas de monitoramento da merenda em escolas públicas no Pará [[1](https://colab-usp.github.io/monitorando-relatorio-fase2/),[2](http://bit.ly/monitorando-sumario)]
  - Santarém: [Movimento Estudantil](https://www.facebook.com/mpepepara/) + MP
  - Belém: UFPA + CGU
  - Ponta de Pedras: MP
- Brasília: CGU
- Belo Horizonte: Prefeitura

---

[![](imgs/ato.jpg){height=500px}](http://porvir.org/especiais/participacao/alunos-fiscalizam-merenda-e-conseguem-garantir-direitos/)

---

![](imgs/reuniao.jpg){height=500px}

---

![](imgs/foto.jpg){height=500px}

---

![](imgs/app.jpg){height=500px}

---

[![](imgs/relatorio.png){height=500px}](https://monitor.promisetracker.org/pt-BR/campaigns/420/share)

---

![](imgs/degusta.jpg){height=500px}

---

![](imgs/reuniao2.jpg){height=500px}

# Fim

Obrigado!

Link para essa apresentação:

[andresmrm.gitlab.io/apre-cm](https://andresmrm.gitlab.io/apre-cm)
