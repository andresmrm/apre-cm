# Apresentação

Apresentação sobre o Cuidando do Meu Bairro e o Monitorando a Cidade.

[Apresentação](https://andresmrm.gitlab.io/apre-cm)

[Página comum](https://andresmrm.gitlab.io/apre-cm/pagina.html)

# Compilar

Precisa de pandoc.

Para compilar uma vez:

	./run.sh
    
Para compilar quando `apre.md` mudar:

    echo apre.md | entr ./run.sh
